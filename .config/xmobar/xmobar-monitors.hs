Config { 
   -- | Appearance 
     font        = "xft:Jetbrains mono:size=10:bold:antialias=true,Symbols Nerd Font:size=12.5"
   , bgColor     = "#1d1f21"
   , alpha       = 243 
   , fgColor     = "#c5c8c6"
   , position    = Static { xpos = 920 , ypos = 4 , width = 442 , height = 28 } 
   , border      = BottomB 
   , borderColor = "#1d1f21"

   -- | Layout
   , sepChar     = "%"   -- Distinguish Character and Executions 
   , alignSep    = "}{"  -- Separator between Left-Right Alignment
   , template    = " <action=`alacritty -e btop`>%coretemp%</action> <fc=#5c6370>|</fc> <action=`pavucontrol`>%XVol%</action> <fc=#5c6370>|</fc> <action=`xfce4-power-manager-settings`>%XBat%</action> <fc=#5c6370>|</fc> <action=`gnome-clocks`>%XTime%</action> <fc=#5c6370>|</fc> <action=`gnome-calendar`>%XDate%</action>  " -- <fc=#5c6370>|</fc>  

   -- | Behaviour 
   , lowerOnStart =     True    -- Send to bottom of window stack on start
   , hideOnStart  =     False   -- Start with window unmapped (hidden)
   , allDesktops  =     True    -- Show on all desktops
   , pickBroadest =     False   -- Choose widest display (multi-monitor)
   , persistent   =     True    -- Enable/disable hiding (True = disabled)
   , iconRoot     =     ".xmonad/icons/"  -- default: "."

   -- | Plugins
   , commands =  
        [ Run CoreTemp       [ "--template" , "<fc=#cc6666><fn=2>﨎</fn></fc> <core0>°C"
                             , "--Low"      , "70"         -- units: °C
                             , "--High"     , "80"         -- units: °C
                             , "--low"      , "#c5c8c6"
                             , "--normal"   , "#c5c8c6"
                             , "--high"     , "#c5c8c6"
                             ] 50
        , Run Com "/bin/bash" ["-c",  ".config/xmobar/Scripts/XMgetvolume.sh" ] "XVol" 10
        , Run Com "/bin/bash" ["-c",  ".config/xmobar/Scripts/XMgetbattery.sh"] "XBat" 10
        , Run Com "/bin/bash" ["-c",  ".config/xmobar/Scripts/XMgettime.sh"   ] "XTime" 10
        , Run Com "/bin/bash" ["-c",  ".config/xmobar/Scripts/XMgetdate.sh"   ] "XDate" 10
        ]
   }
