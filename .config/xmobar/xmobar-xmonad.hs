Config { 
   -- | Appearance 
     font        = "xft:Jetbrains mono:size=10:bold:antialias=true,Symbols Nerd Font:size=12.5"
   , bgColor     = "#1d1f21"
   , alpha       = 243 
   , fgColor     = "#c5c8c6"
   , position    = Static { xpos = 40 , ypos = 4, width = 300, height = 28 } 
   , border      = BottomB 
   , borderColor = "#1d1f21"

   -- | Layout
   , sepChar     = "%"   -- Distinguish Character and Executions 
   , alignSep    = "}{"  -- Separator between Left-Right Alignment
   , template    = " %UnsafeStdinReader% " -- <fc=#5c6370>|</fc>  

   -- | Behaviour 
   , lowerOnStart =     True    -- Send to bottom of window stack on start
   , hideOnStart  =     False   -- Start with window unmapped (hidden)
   , allDesktops  =     True    -- Show on all desktops
   , pickBroadest =     False   -- Choose widest display (multi-monitor)
   , persistent   =     True    -- Enable/disable hiding (True = disabled)
   , iconRoot     =     ".xmonad/icons/"  -- default: "."

   -- | Plugins
   , commands = [
        Run UnsafeStdinReader
        ]
   }
