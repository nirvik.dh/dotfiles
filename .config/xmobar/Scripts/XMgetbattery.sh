#!/bin/bash
battery_Percentage=$(upower -i $(upower -e | grep BAT) | grep -E "percentage" | awk '{print $2}' | tr -d '%')
battery_Status=$(upower -i $(upower -e | grep BAT) | grep --color=never -E "state" | awk '{print $2}')

if [ "$battery_Status" = "charging" ]
  then echo "<fc=#b294bb><fn=2> </fn></fc><fc=#c5c8c6>$battery_Percentage%</fc>"
elif [ "$battery_Percentage" -ge 80 ]
  then echo "<fc=#b294bb><fn=2> </fn></fc><fc=#c5c8c6>$battery_Percentage%</fc>"
elif [ "$battery_Percentage" -ge 60 ]
  then echo "<fc=#b294bb><fn=2> </fn></fc><fc=#c5c8c6>$battery_Percentage%</fc>"
elif [ "$battery_Percentage" -ge 40 ]
  then echo "<fc=#b294bb><fn=2> </fn></fc><fc=#c5c8c6>$battery_Percentage%</fc>"
elif [ "$battery_Percentage" -ge 20 ]
  then echo "<fc=#b294bb><fn=2> </fn></fc><fc=#c5c8c6>$battery_Percentage%</fc>"
elif [ "$battery_Percentage" -lt 20 ]
  then echo "<fc=#b294bb><fn=2> </fn></fc><fc=#c5c8c6>$battery_Percentage%</fc>"
fi 

exit 0 
