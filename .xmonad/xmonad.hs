import Colors.TomorrowNight
import qualified Data.Map as M
import Data.Monoid
import System.Exit
import XMonad

import XMonad.Actions.OnScreen
import XMonad.Actions.Submap
import XMonad.Actions.UpdatePointer
import XMonad.Actions.WindowMenu

import XMonad.Hooks.DynamicLog
import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.Minimize
import XMonad.Hooks.SetWMName

import XMonad.Layout.CenteredMaster
import XMonad.Layout.Grid
import XMonad.Layout.LayoutModifier
import XMonad.Layout.NoBorders
import XMonad.Layout.Renamed
import XMonad.Layout.ResizableTile
import XMonad.Layout.Simplest
import XMonad.Layout.Spacing
import XMonad.Layout.Spiral
import XMonad.Layout.SubLayouts
import XMonad.Layout.Tabbed
import XMonad.Layout.WindowNavigation
import XMonad.Layout.SimpleDecoration

import qualified XMonad.StackSet as W
import XMonad.Util.EZConfig
import XMonad.Util.Run
import XMonad.Util.SpawnOnce

--------------- CONFIGURATION ---------

-- | Application
myTerminal = "alacritty -vvv"
myRunLauncher = "dmenu_run -h 24"
myFileManager = "thunar "
myBrowser = "brave"
myLocker = "slock"
myWallpChanger = "feh --bg-fill --randomize ~/Customization/wallpapers"

-- | DMenu Scripts
dmLogoutConfirm = "~/Scripts/dmenu/control-user-options.sh"

-- | Rofi
rofiLauncher = "~/.config/rofi/launcher/launcher.sh"
rofiSpotify = "~/.config/rofi/multimedia/spotify-controls.sh"

-- | Focus
myFocusFollowsMouse :: Bool
myFocusFollowsMouse = False

myClickJustFocuses :: Bool
myClickJustFocuses = False

-- | Border
myBorderWidth = 2
myNormalBorderColor = colorBack
myFocusedBorderColor = color02

-- | Key Mask
myModMask = mod4Mask

-- | Workspaces & Windows
myWorkspaces = ["\59285", "\61563", "\62057", "\63080", "\63257", "\62866", "\61694"]
winCount :: X (Maybe String)
winCount = gets $ Just . show . length . W.integrate' . W.stack . W.workspace . W.current .windowset 

-- | Layout
mySpacing :: Integer -> l a -> XMonad.Layout.LayoutModifier.ModifiedLayout Spacing l a
mySpacing i = spacingRaw False (Border i i i i) False (Border i i i i) True

myLayoutPrinter :: String -> String
myLayoutPrinter "tall" = "\64319"
myLayoutPrinter "grid" = "\61449"
myLayoutPrinter "full" = "\62795"
myLayoutPrinter "spirals" = "\63179"
myLayoutPrinter "tabs" = "\61451"

myTabTheme =
  def
    { fontName = "xft:Jetbrains Mono:size=8:bold",
      activeColor = color12,
      inactiveColor = color01,
      activeBorderColor = color02,
      inactiveBorderColor = color01,
      activeTextColor = color01,
      inactiveTextColor = colorFore
    }

--------------- KEY BINDINGS ----------
myKeys =
  [ -- Launcher
    ("M-S-<Return>", spawn myTerminal), -- Launch Terminal
    ("M-p", spawn myRunLauncher), -- Launch Run Launcher
    ("M-r", spawn rofiLauncher), -- Launch Rofi Launcher
    ("M-e", spawn myFileManager), -- Launch File Manager
    ("M-b", spawn myBrowser), -- Launch Web Browser
    ("M-S-l", spawn myLocker), -- Launch Light Locker
    ("M-C-n", spawn myWallpChanger), -- Next Wallpaper

    -- Function Application Launchers
    ("M-<F1>", spawn "virtualbox"), -- Launch VBOX
    ("M-<F2>", spawn "font-manager"), -- Launch Font Manager
    ("M-<F3>", spawn "code"), -- Launch Visual Studio Code
    ("M-<F4>", spawn "spotify"), -- Launch Spotify
    ("M-<F5>", spawn "gimp"), -- Launch GIMP
    ("M-<F6>", spawn "rednotebook"), -- Launch RedNoteBook
    ("M-<F7>", spawn "brave"), -- Launch Brave Browser
    ("M-<F8>", spawn "caprine"), -- Launch Caprine
    ("M-<F9>", spawn "mailspring"), -- Launch MailSpring

    -- SUBMAP :- System
    ( "M-C-s",
      submap . M.fromList $
        [ ((0, xK_r), spawn "reboot"), -- Reboot
          ((0, xK_s), spawn "shutdown -h now"), -- Shutdown
          ((0, xK_l), spawn "slock") -- Lock
        ]
    ),
    -- SUBMAP :- Rofi
    ( "M-S-r",
      submap . M.fromList $
        [ ((0, xK_s), spawn rofiSpotify) -- Spotify
        ]
    ),
    -- Layouts
    ("M-<Space>", sendMessage NextLayout), -- Next Layout
    ("M-h", sendMessage Shrink), -- Shrink Left
    ("M-l", sendMessage Expand), -- Expand Left
    ("M-f", sendMessage ToggleStruts), -- Full Screen
    ("M-C--", decWindowSpacing 1), -- Decrease Window Spacing 1
    ("M-C-=", incWindowSpacing 1), -- Increase Window Spacing 1
    ("M-C-h", sendMessage $ pullGroup L), -- Tab Controls
    ("M-C-l", sendMessage $ pullGroup R), -- Tab Controls
    ("M-C-k", sendMessage $ pullGroup U), -- Tab Controls
    ("M-C-j", sendMessage $ pullGroup D), -- Tab Controls
    ("M-C-m", withFocused (sendMessage . MergeAll)),
    ("M-C-u", withFocused (sendMessage . UnMergeAll)),
    ("M-C-.", onGroup W.focusUp'), -- Switch focus to next tab
    ("M-C-,", onGroup W.focusDown'), -- Switch focus to prev tab

    -- Screen
    ("M-C-1", windows (viewOnScreen 0 "1")), -- Focus To Screen 0
    ("M-C-2", windows (viewOnScreen 1 "1")), -- Focus To Screen 1

    -- Focus & Swap
    ("M-j", windows W.focusDown), -- Next Window
    ("M-S-j", windows W.swapDown), -- Swap Next
    ("M-k", windows W.focusUp), -- Previous Window
    ("M-S-k", windows W.swapUp), -- Swap Previous
    ("M-m", windows W.focusMaster), -- Focus Master
    ("M-S-m", windows W.swapMaster), -- Swap Master
    ("M-,", sendMessage (IncMasterN 1)), -- Increase Masters - 1
    ("M--", sendMessage (IncMasterN (-1))), -- Decrease Masters - 1

    -- Application Actions
    ("M-t", withFocused $ windows . W.sink), -- Force Back to Tiling
    ("M-S-x", spawn "xkill"), -- Select to kill
    ("M-w", windowMenu), -- Bring Window Menu
    ("M-S-c", kill), -- Select to kill

    -- System
    ("M-q", spawn "xmonad --recompile; xmonad --restart"), -- Restart XMonad
    ("M-S-q", spawn "pkill xmonad "), -- Quit

    -- Media Controls
    ("<XF86AudioRaiseVolume>", spawn "amixer sset Master 3%+"), -- Increase Volume 3%
    ("<XF86AudioLowerVolume>", spawn "amixer sset Master 3%-"), -- Decrease Volume 3%
    ("<XF86AudioMute>", spawn "amixer sset Master toggle"), -- Toggle Mute / Unmute
    ("S-<XF86AudioMute>", spawn "amixer sset Master 25%"), -- Set Volume to 25
    ("<XF86AudioMicMute>", spawn "amixer sset Capture toggle"), -- Toggle Mic Mute / Unmute
    ("S-<XF86AudioMicMute>", spawn "amixer sset Capture 50%"), -- Set Mic to 50
    ("<Print>", spawn "flameshot gui"), -- Capture Selected Area
    ("S-<Print>", spawn "flameshot launcher") -- Options to Capture
  ]

myKeysd conf@(XConfig {XMonad.modMask = modm}) =
  M.fromList $
    [ ((m .|. modm, k), windows $ f i)
      | (i, k) <- zip (XMonad.workspaces conf) [xK_1 .. xK_7],
        (f, m) <- [(W.greedyView, 0), (W.shift, shiftMask)]
    ]

--------------- MOUSE BINDINGS --------
myMouseBindings (XConfig {XMonad.modMask = modm}) =
  M.fromList
    [ ( (modm, button1),
        \w ->
          focus w >> mouseMoveWindow w
            >> windows W.shiftMaster -- Set the window to floating mode and move by dragging
      ),
      ((modm, button2), \w -> focus w >> windows W.shiftMaster), -- Raise the window to the top of the stack
      ( (modm, button3),
        \w ->
          focus w >> mouseResizeWindow w
            >> windows W.shiftMaster -- Set the window to floating mode and resize by dragging
      )
    ]

--------------- LAYOUTS ---------------
tall =
  renamed [Replace "tall"] $
    smartBorders $
      windowNavigation $
        addTabs shrinkText myTabTheme $
          mySpacing 4 $
            ResizableTall 1 (3 / 100) (1 / 2) []

grid =
  renamed [Replace "grid"] $
    smartBorders $
      windowNavigation $
        mySpacing 4 $
          GridRatio (16 / 10)

full =
  renamed [Replace "full"] $
    smartBorders $
      windowNavigation $
        addTabs shrinkText myTabTheme $
          mySpacing 4 $
--            simpleDeco shrinkText def $
              Full

spirals =
  renamed [Replace "spirals"] $
    smartBorders $
      windowNavigation $
        mySpacing 4 $
          spiral (6 / 7)

tabs =
  renamed [Replace "tabs"] $
    smartBorders $
      windowNavigation $
        tabbed shrinkText myTabTheme

myLayout = avoidStruts (full ||| tabs ||| tall ||| grid ||| spirals)

--------------- MANAGE HOOKS ----------
myManageHook =
  composeAll
    [ className =? "MPlayer" --> doFloat,
      className =? "VirtualBox Manager" --> doFloat,
      resource =? "desktop_window" --> doIgnore,
      resource =? "kdesktop" --> doIgnore,
      className =? "Brave" --> doCenterFloat,
      className =? "Nitrogen" --> doCenterFloat,
      className =? "flameshot" --> doCenterFloat,
      className =? "VirtualBox Manager" --> doShift (head myWorkspaces),
      className =? "Font-manager" --> doShift (myWorkspaces !! 1)
    ]

--------------- EVENT HOOKS -----------
myEventHook = mempty

--------------- STARTUP HOOKS ---------
myStartupHook = do
  -- spawnOnce "lxsession"
  spawnOnce "/usr/lib/polkit-gnome/polkit-gnome-authentication-agent-1"
  spawnOnce "xfce4-power-manager"
  -- spawnOnce "/usr/lib/mate-polkit/polkit-mate-authentication-agent-1"
  -- spawnOnce "mate-power-manager"
  spawnOnce "feh --bg-fill --randomize ~/Customization/wallpapers/"
  spawnOnce "parcellite"
  spawnOnce "picom --config ~/.config/picom/picom.conf"
  spawnOnce "redshift-gtk"
  spawnOnce "nm-applet"
  spawnOnce "gnome-clocks"
  spawnOnce "conky ~/.config/conky/conky.conf"
  spawn ("sleep 2 && trayer --edge top --distance 4 --distancefrom top --align center --widthtype request --padding 6 --SetDockType true --SetPartialStrut true --expand true --transparent true --alpha 1 " ++ colorTrayer ++ " --height 28")
  setWMName "LG3D"

--------------- MAIN ------------------
main = do
  xmproc0 <- spawnPipe ("xmobar -x 0 ~/.config/xmobar/xmobar-search.hs")
  xmproc1 <- spawnPipe ("xmobar -x 0 ~/.config/xmobar/xmobar-xmonad.hs")
  xmproc2 <- spawnPipe ("xmobar -x 0 ~/.config/xmobar/xmobar-monitors.hs")
  xmonad $
    docks $
      ewmhFullscreen $
        ewmh $
          def
            { -- CONFIGURATION
              terminal = myTerminal,
              focusFollowsMouse = myFocusFollowsMouse,
              clickJustFocuses = myClickJustFocuses,
              borderWidth = myBorderWidth,
              modMask = myModMask,
              workspaces = myWorkspaces,
              normalBorderColor = myNormalBorderColor,
              focusedBorderColor = myFocusedBorderColor,
              -- BINDINGS
              keys = myKeysd,
              mouseBindings = myMouseBindings,
              -- HOOKS & LAYOUTS
              layoutHook = myLayout,
              manageHook = myManageHook,
              handleEventHook = myEventHook,
              logHook =
                dynamicLogWithPP $
                  xmobarPP
                    { ppOutput = hPutStrLn xmproc1, -- xmobar on monitor 1
                      ppCurrent = xmobarColor color12 "" . wrap "[ " " ]",
                      ppVisible = xmobarColor color12 "",
                      ppHidden = xmobarColor color05 "",
                      ppTitleSanitize  = xmobarColor color05 "",
                      ppUrgent = xmobarColor color02 "" . wrap "! " " !",
                      ppLayout = xmobarColor color05 "" . myLayoutPrinter,
                      ppSep = "<fc=" ++ color09 ++ "><fn=1> |</fn> </fc>",
                      ppExtras = [ winCount], 
                      ppOrder = \(ws : l : t : ex) -> [ws, l]++ex,
                      ppHiddenNoWindows = xmobarColor color09 ""
                    },
              startupHook = myStartupHook
            }
            `additionalKeysP` myKeys
