# Dotfiles
![Xmonad](https://res.cloudinary.com/nirvikdh/image/upload/v1663251070/Linux/xmonad-002_iioaqt.png)
* * *
## Table Of Content
- Window Manager :- Xmonad
- Terminal Emulator :- Alacritty 
- Run Launcher :- Rofi 
- Browser :- Brave Beta

## Others :- 
- [Wallpapers](https://gitlab.com/nirvik.dh/wallpapers)
- [Scripts](https://gitlab.com/nirvik.dh/scripts)
