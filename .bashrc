#  _   _ ____
# | \ | |  _ \
# |  \| | | | |    ~/.bashrc
# | |\  | |_| |
# |_| \_|____/
# 

[[ $- != *i* ]] && return

#PS1='[\u@\h \W]\$ ' # Disabled if Starship enabled

## SOURCES
source ~/Scripts/bash/all.sh

## ARCHIVE EXTRACTION
# usage: ex <file>
ex ()
{
  if [ -f "$1" ] ; then
    case $1 in
      *.tar.bz2)   tar xjf $1   ;;
      *.tar.gz)    tar xzf $1   ;;
      *.bz2)       bunzip2 $1   ;;
      *.rar)       unrar x $1   ;;
      *.gz)        gunzip $1    ;;
      *.tar)       tar xf $1    ;;
      *.tbz2)      tar xjf $1   ;;
      *.tgz)       tar xzf $1   ;;
      *.zip)       unzip $1     ;;
      *.Z)         uncompress $1;;
      *.7z)        7z x $1      ;;
      *.deb)       ar x $1      ;;
      *.tar.xz)    tar xf $1    ;;
      *.tar.zst)   unzstd $1    ;;
      *)           echo "'$1' cannot be extracted via ex()" ;;
    esac
  else
    echo "'$1' is not a valid file"
  fi
}

## SHOPT
shopt -s autocd

## ALIASES
# Navigation
alias ls='exa -a --icons'
alias la='exa -a --icons'
alias ll='exa -lah --icons'

# Addding Flags
alias cp='cp -i'
#alias rm='rm -i'
alias df='df -h'

# Custom Commands
alias w3schoolrun='docker run -d -p 127.0.0.1:80:80 ghcr.io/ja7ad/w3schools'
alias doom='~/.emacs.d/bin/doom'

# Replace Commands 
alias cat='bat'
alias vim='nvim'

## EXPORT
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export EDITOR="nvim"
export HISTCONTROL=ignoredups:erasedups           # no duplicate entries

## STARSHIP
eval "$(starship init bash)"



[ -f ~/.fzf.bash ] && source ~/.fzf.bash
