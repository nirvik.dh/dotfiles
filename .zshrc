# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd
bindkey -e
# End of lines configured by zsh-newuser-install
# The following lines were added by compinstall
zstyle :compinstall filename '/home/nirvik/.zshrc'

autoload -Uz compinit
compinit
# End of lines added by compinstall
eval "$(starship init zsh)"

## ALIASES
# Navigation
alias ls='exa -a --icons'
alias la='exa -a --icons'
alias ll='exa -lah --icons'

# Addding Flags
alias cp='cp -i'
alias rm='rm -i'
alias df='df -h'

# Custom Commands
alias w3schoolrun='docker run -d -p 127.0.0.1:80:80 ghcr.io/ja7ad/w3schools'

# Replace Commands
alias cat='bat'
alias vim='nvim'

## EXPORT
export MANPAGER="sh -c 'col -bx | bat -l man -p'"
export EDITOR="nvim"
export HISTCONTROL=ignoredups:erasedups           # no duplicate entries

## SOURCES 
source ~/Scripts/zsh/zsh-autosuggestions/zsh-autosuggestions.zsh
source ~/Scripts/zsh/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh
